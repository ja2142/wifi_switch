#include<stdbool.h>

#include <etstimer.h>

#define SWITCH_CHANNELS 2
#define SWITCH_TIME_MS 500

typedef struct{
    int state;
    bool timer_armed[2];
    int gpio[2];
    ETSTimer timer[2];
}switch_t;

static switch_t switches[SWITCH_CHANNELS] = {
    {.gpio ={14,16} },
    {.gpio ={13,12} },
};

void gpio_off(void* argp){
    int arg = (int)argp;
    int state = arg & 1;
    int switch_no = arg>>1;
    printf("gpio off: state: %i, switch_no: %i\n", state, switch_no);
    gpio_write(switches[switch_no].gpio[state], 0);
    switches[switch_no].timer_armed[state] = false;
}

void p_switch_init(){
    int i;
    for(i=0; i< SWITCH_CHANNELS; i++){
        switches[i].state = 0;
        switches[i].timer_armed[0] = false;
        switches[i].timer_armed[1] = false;
        gpio_enable(switches[i].gpio[0], GPIO_OUTPUT);
        gpio_enable(switches[i].gpio[1], GPIO_OUTPUT);
        sdk_ets_timer_setfn(&switches[i].timer[0], gpio_off, (void*)(i<<1 | 0));
        sdk_ets_timer_setfn(&switches[i].timer[1], gpio_off, (void*)(i<<1 | 1));
        gpio_write(switches[i].gpio[0], 0);
        gpio_write(switches[i].gpio[1], 0);
    }
    //TODO set both to 0 (or not?)
    p_switch_set(0, 0);
    p_switch_set(1, 0);
}

int p_switch_get(int switch_no){
    return switches[switch_no].state;
}

int p_switch_toggle(int switch_no){
    if(switches[switch_no].timer_armed[0] || switches[switch_no].timer_armed[1]){
        // switch is already toggling, skip this toggle
    } else {
        int state = switches[switch_no].state;

        gpio_write(switches[switch_no].gpio[state], 1);
        sdk_ets_timer_arm(&switches[switch_no].timer[state], 
            SWITCH_TIME_MS, false);
        switches[switch_no].timer_armed[state] = true;
        switches[switch_no].state = (state+1) %2;
    }
    return p_switch_get(switch_no);
}

int p_switch_set(int switch_no, int state){
    if(switches[switch_no].state != state){
        p_switch_toggle(switch_no);
    }
    return p_switch_get(switch_no);
}

