#ifndef _POWER_SWITCH_H
#define _POWER_SWITCH_H

void p_switch_init();

int p_switch_get(int switch_no);

int p_switch_set(int switch_no, int state);

int p_switch_toggle(int switch_no);

#endif
